package com.example.excel.controller;

import com.example.excel.com.EmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import com.example.excel.com.Student;
import com.example.excel.com.StudentRepository;

@Controller
@RequestMapping("/students")
public class StudentController {

    @Autowired
    private StudentRepository studentRepository;

    @Autowired
    private EmailService emailService;

    @GetMapping
    public String getAllStudents(Model model) {
        model.addAttribute("students", studentRepository.findAll());
        return "student-list";
    }

    @PostMapping
    public String addStudent(@ModelAttribute Student student) {
        studentRepository.save(student);
        return "redirect:/students";
    }

    @PostMapping("/send-email")
    public String sendEmail(@RequestParam("email") String email) {
        Student student = studentRepository.findByEmail(email);
        if (student != null) {
            String subject = "Demo gửi email";
            String text = "Xin chào " + student.getFirstName() + ", \nChúc mừng bạn trúng tuyển vào đại học FPT, mọi thắc mắc liên hệ đến nhà trường";
            emailService.sendEmail(email, subject, text);
            return "redirect:/students";
        } else {
            return "redirect:/students";
        }
    }


}










